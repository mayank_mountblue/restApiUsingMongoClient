var express = require("express");
var router = express.Router();
var mongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/todoData";
var data;
router.get('/', function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true
    }, function (err, db) {
        if (err) throw err;
        data = db.db("todoData");
        data.collection("todoData").find().toArray(function (err, result) {
            if (err) throw err;
            console.log(result);
            res.send(result);
            db.close();
        });
    });

});

router.get("/:id", function (req, res) {
   
    mongoClient.connect(url, {
        useNewUrlParser: true
    }, function (err, db) {
        if (err) throw err;
        data = db.db("todoData");

        var query = {
            _id: parseInt(req.params.id)
        };
       
        data.collection("todoData").find(query).toArray(function (err, result) {
           res.send(result);
         
            db.close();
        });

    });
});
router.post('/', function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true
    }, function (err, db) {

        if (err) throw err;
        var id ;
        data = db.db("todoData");
     data.collection("todoData").find({}).toArray(function(err, result){
       id = result[result.length-1]._id +1;
       console.log(id);
     });
     var obj = {
        _id:id,
     "text": req.body.name,
     "done": req.body.done
 };
 
 data.collection("todoData").insertOne(obj,function (err, res){
    if(err) throw err;
   

});
db.close();

});
res.send("record added");
});
router.put("/:id", function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true
    }, function (err, db) {
        var query = {
            _id: parseInt(req.params.id)
        };   
        data = db.db("todoData");
        var newvalues = { $set: {"text": "lunch", "done":false } };
        if (err) throw err;
        data.collection("todoData").updateOne(query,newvalues,function(err, res){
       if(err) throw err;
    
        });
});
res.send("updated");
});

router.delete("/:id", function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true
    }, function (err, db) {
        if (err) throw err;
        data = db.db("todoData");

        var query = {
            _id: parseInt(req.params.id)
        };
       
        data.collection("todoData").deleteOne(query, function(err, obj){
           res.send("1 document delelted");
         
            db.close();
        });

    });

    
});
module.exports = router;